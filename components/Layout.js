import Link from 'next/link'
import { Container, Row, Col, Nav}  from 'react-bootstrap'

const Layout = ({ children, title }) => (
<>
	
	<div className="root">

	

	
			

			<h1>{title}</h1>

			{children}

			<footer>&copy; {new Date().getFullYear()}</footer>
			<style jsx>{`
    .root {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }
    .parallax {
  
  background-image: url("img_parallax.jpg");

  
  min-height: 500px; 

 
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}
    header {
        width: 100%;
        display: flex;
        justify-content: space-around;
        padding: 1em;
        font-size: 1.2rem;
        background: #009999
    }
    header a {
        color: white;
        text-decoration: none;
    }
    header a:hover {
        font-weight: bold;
        color: white;
    }
    footer {
        padding: 1em;
    }
`}</style>


	</div>	
	</>
)

export default Layout

// <header>
// 				<Link href="/"><a>Home</a></Link>
// 				<Link href="/portfolio"><a>Portfolio</a></Link>

// 			</header>
