import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { motion } from "framer-motion"
import Layout from '../components/Layout'
import { Button } from 'react-bootstrap';
import Link from 'next/link'
import { Container, Row, Col, Card, CardDeck } from 'react-bootstrap'

import React from 'react';


export default function Home() {
  return (

        <React.Fragment>
                <Container>

                  <Row className="text-center">
                    <Col sm={1}><img src="/myLogo.png" className="rounded-circle float-left mr-4 mt-4" alt="Cinque Terre" style={{height: 100, width: 100}}></img></Col>
                    <Col sm={11}><motion.div initial="hidden" animate="visible"
                                        variants={{ hidden: {
                                                    scale: .8,
                                                    opacity: 0
                                                  }, visible: {
                                                    scale: 1,
                                                    opacity: 1,
                                                    transition: {
                                                    delay: .6
                                                  }
                                                },
                                              }}>

                                  <h1 className="display-1 text-white font-weight-bold mt-5">Welcome to my Website</h1>

                                </motion.div></Col>
                  </Row>
                  <Row>
                    <Col>
                                <div className="jumbotron text-center jump">
                                  
                                  <hr className="my-4"></hr>
                                      <a className="btn btn-primary btn-lg bg-danger" href="/portfolio" role="button">Jump to my Portfolio</a>
                                  <hr className="my-4"></hr>
                                </div>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                        <div className="intro px-5 py-5">
                                  <p>
                                       I am using React + Next.js for this Website. If you wanna learn more about the React and Nextjs you can check out the link below and make that you are using the new version of each packages to avoid double work effort trying to redo everything you did because when you try to deploy your project conflicts may occur...
                                  </p>
                        </div>
                    </Col>
                  </Row>



                  <Row className="mt-5">
                    <CardDeck>
                        <Card className="bg-dark next-card" style={{ opacity: .9 }}>
                          
                          <Card.Body>
                            <Card.Text>
                              <Link href="https://nextjs.org/docs">
                                  <a>
                                      <h3>Documentation &rarr;</h3>
                                      <p>Find in-depth information about Next.js features and API.</p>
                                  </a>
                            </Link>
                            </Card.Text>
                          </Card.Body>
                        </Card>

                        <Card className="bg-dark next-card" style={{ opacity: .9 }}>
                          
                          <Card.Body>
                            
                            <Card.Text>
                              <Link  href="https://nextjs.org/learn">
                                    <a>
                                      <h3>Learn &rarr;</h3>
                                      <p>Learn about Next.js in an interactive course with quizzes!</p>
                                    </a>
                              </Link>
                            </Card.Text>
                          </Card.Body>
                          
                        </Card>

                        <Card className="bg-dark next-card" style={{ opacity: .9 }}>
                          
                          <Card.Body>
                           
                            <Card.Text>
                              <Link href="https://github.com/vercel/next.js/tree/master/examples">           
                                  <a>
                                  <h3>Examples &rarr;</h3>
                                  <p>Discover and deploy boilerplate example Next.js projects.</p>
                                </a>
                              </Link>
                            </Card.Text>
                          </Card.Body>
                          
                        </Card>

                        <Card className="bg-dark next-card" style={{ opacity: .9 }}>
                          
                          <Card.Body>
                           
                            <Card.Text>
                              <Link href="https://vercel.com/import?filter=next.js&utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app">                       
                                   <a>
                                    <h3>Deploy &rarr;</h3>
                                    <p>
                                      Instantly deploy your Next.js site to a public URL with Vercel.
                                    </p>
                                  </a>
                              </Link>

                            </Card.Text>
                          </Card.Body>
                          
                        </Card>
                      </CardDeck>
                  </Row>
                  <Row>
                    <Col>1 of 1</Col>
                  </Row>
                </Container>

              <style jsx>
                     {`

                      .root {
                       
                        background-image: url("/city.gif");
                        
                      }
                      .display-1 {
                                text-shadow: 2px 2px red;
                            }
                       .jump {
                          background: none;
                      }
                      .intro {
                          background: grey;
                          opacity: .8;
                          border-radius: 10px;
                          text-shadow: 1px 1px red;
                      }
                      


                      `}
                </style>


                <style global jsx>
                     {`

                      html {
                        background-image: url("/city.gif");
                        scroll-behavior: smooth;
                        
                        
                      }
                      body{
                        background-image: url("/city.gif");
                      }
                      `}
                </style>



        </React.Fragment>

  )
}


