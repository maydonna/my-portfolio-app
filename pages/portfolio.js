import Layout from '../components/Layout'
import { Parallax, Background } from 'react-parallax';
import Link from 'next/link'

import { motion } from "framer-motion"


export default function Portfolio() {

	
  return (
	<>

		<div className="container-fluid root">

			<div className="row">
			
					  
					
			<div className="col-6 col-md-2 container pl-2 mt-5 myNav">
			
					<div className="card container border-0 mt-3">

					  <img className="card-img-top" src="/forresume.png" alt="Card image cap"></img>
					  
					</div>

					
				<ul className="nav flex-column mt-3 ">
				  <li className="nav-item">
				  <Link href="/"><a className="nav-link active text-white">Home</a></Link>
				    
				  </li>
				  <li className="nav-item">
				    <Link href="#capstones"><a className="nav-link active text-white">Capstones</a></Link>
				  </li>
				  <li className="nav-item">
				    <Link href="#sideProjects"><a className="nav-link active text-white">Side Projects</a></Link>
				  </li>
				  <li className="nav-item">
				    <Link href="/"><a className="nav-link active text-white">Upcoming blogs</a></Link>
				  </li>
				</ul>
			</div>
		    <div className="col-md-9">
		    	<div className="parallax">
		    	<img src="/myLogo.png" className="rounded-circle float-right mr-4 mt-4" alt="Cinque Terre" style={{height: 100, width: 100}}></img>
		    		<div className="text-white jumbotron">
		    		
		    		<motion.div initial="hidden" animate="visible" variants={{
        hidden: {
          scale: .8,
          opacity: 0
        }, visible: {
          scale: 1,
          opacity: 1,
          transition: {
            delay: .6
          }
        },
      }}>
		    		<h2 className="display-4">Hello, World!</h2>
		    		<h1 className="display-3"><strong>I'm Donna, Web Developer</strong></h1>


		    		</motion.div>
				
					</div>
				</div>

		    </div>   
		  </div>

		  

		


		 

		 <div className="container mt-5 mb-5 capstones" id="capstones">
		  <div className="card-group">
		  <div className="card border-0 mt-5">
		    <img src="/myLogo.png" className="card-img-top container" alt="..."></img>
		    <div className="card-body">
		      <h5 className="card-title text-white text-center">CAPSTONE 1</h5>
		      <p className="card-text text-white">
		      		<Link href="https://tuitt.gitlab.io/students/batch40/awopegba/capstone1/" passHref>
		      			<a target="_blank"><button type="button" className="btn btn-outline-danger text-white">Live demo</button></a>
					  
					</Link>

			</p>
			<p className="card-text text-white">
		      		<Link href="https://tuitt.gitlab.io/students/batch40/awopegba/capstone1/" passHref>
		      			<a target="_blank"><button type="button" className="btn btn-outline-primary text-white">Git repo</button></a>
					  
					</Link>

			</p>
		    </div>
		    <div className="card-footer">
		      <small className="text-muted"></small>
		    </div>
		  </div>
		  <div className="card border-0 mt-5">
		    <img src="/myLogo.png" className="card-img-top container" alt="..."></img>
		    <div className="card-body">
		      <h5 className="card-title text-white text-center">CAPSTONE 2</h5>
		      <p className="card-text text-white">
		      		<Link href="https://maydonna.gitlab.io/capstone-2/" passHref>
		      			<a target="_blank"><button type="button" className="btn btn-outline-danger text-white">Live demo</button></a>
					  
					</Link>

			</p>
			<p className="card-text text-white">
		      		<Link href="https://tuitt.gitlab.io/students/batch40/awopegba/capstone1/" passHref>
		      			<a target="_blank"><button type="button" className="btn btn-outline-primary text-white">Git repo</button></a>
					  
					</Link>

			</p>
		    </div>
		    <div className="card-footer">
		      <small className="text-muted"></small>
		    </div>
		  </div>
		  <div className="card border-0 mt-5">
		    <img src="/myLogo.png" className="card-img-top container" alt="..."></img>
		    <div className="card-body">
		      <h5 className="card-title text-white text-center">CAPSTONE 3</h5>
		      <p className="card-text text-white">
		      		<Link href="https://my-tracker-expense.herokuapp.com/" passHref>
		      			<a target="_blank"><button type="button" className="btn btn-outline-danger text-white">Live demo</button></a>
					  
					</Link>

			</p>
			<p className="card-text text-white">
		      		<Link href="https://tuitt.gitlab.io/students/batch40/awopegba/capstone1/" passHref>
		      			<a target="_blank"><button type="button" className="btn btn-outline-primary text-white">Git repo</button></a>
					  
					</Link>

			</p>
		    </div>
		    <div className="card-footer">
		      <small className="text-muted"></small>
		    </div>
		  </div>
		</div>
		</div>




		    	<div className="container-fluid" id="sideProjects">
		    	<div className="parallax">
		    		<div className="text-white jumbotron text-center">
		    		<h2 className="display-4"></h2>
		    		<motion.div initial="hidden" animate="visible" variants={{
        hidden: {
          scale: .8,
          opacity: 0
        }, visible: {
          scale: 1,
          opacity: 1,
          transition: {
            delay: .6
          }
        },
      }}>
		    		<h1 className="display-3 mt-5 pt-5"><strong>Side projects here comming soon!</strong></h1>
	</motion.div>
				
					</div>
				</div>

		    </div> 


{/**footer**/}		    


		<div className="container-fluid mt-5 mb-5">
		 <div className="col-md-12">
		    	<div className="parallax-a">
		    		<div className="text-white jumbotron">
		    		<div className="card-group">
		  <div className="card border-0">
		    <img src="/myLogo.png" className="card-img-top container" alt="..."></img>
		    <div className="card-body">
		      <h5 className="card-title">Icons footer</h5>
		      <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
		    </div>
		    <div className="card-footer">
		      <small className="text-muted"></small>
		    </div>
		  </div>
		  <div className="card border-0">
		    <img src="/myLogo.png" className="card-img-top container" alt="..."></img>
		    <div className="card-body">
		      <h5 className="card-title">Icons footer</h5>
		      <p className="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
		    </div>
		    <div className="card-footer">
		      <small className="text-muted"></small>
		    </div>
		  </div>
		  <div className="card border-0">
		    <img src="/myLogo.png" className="card-img-top container" alt="..."></img>
		    <div className="card-body">
		      <h5 className="card-title">Icons footer</h5>
		      <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
		    </div>
		    <div className="card-footer">
		      <small className="text-muted"></small>
		    </div>
		  </div>
		</div>
					</div>
				</div>

		    </div> 

		 </div>





		</div>




		<style jsx>{`
		.root{
			
			background-image: url("/J4o.gif");

		}
		.parallax {
		  
		  background-image: linear-gradient(97deg, rgba(207,7,121,0.4430147058823529) 0%, rgba(111,19,37,1) 100%), url("/xample2.png");
		  min-height: 500px; 
		  background-attachment: fixed;
		  background-position: center;
		  background-repeat: no-repeat;
		  background-size: cover;
		},
		.jumbotron {
		    background: none
		}
		
		.lala{
			background-image: linear-gradient(97deg, rgba(207,7,121,0.4430147058823529) 0%, rgba(111,19,37,1) 100%)
		}

		.parallax-a {
			background-image: linear-gradient(97deg, rgba(207,7,121,0.4430147058823529) 0%, rgba(111,19,37,1) 100%), url("/keyboard2.gif");
			min-height: 500px; 
		  background-attachment: fixed;
		  background-position: center;
		  background-repeat: no-repeat;
		  background-size: cover;
		}
		.myNav{
			background: #08495e;
		}
		.capstones {
			background: #08495e;
		}
		.addd {
 
  width: 150px
}

.nav-link:hover {
	font-size: 110%
	color: white;
  text-shadow: 1px 1px 2px blue, 0 0 25px red, 0 0 5px darkred;
}

	.card-img-top {
		border-radius: 10px
	}
	
	
	


			`}</style>

			<style global jsx>
		   {`

		   	html {
		   		scroll-behavior: smooth;
		   		background-image: url("/J4o.gif");
		   	}
		       
		  `}</style>
		   	
		</>
	)
}
